//Create slideshow objects for each slideshow? Attach and object to each slideshow?
$(".slideDots").each(function()
{
	$(this).data("m_timeoutHandle", null);
	showSlide(1, $(this).children(":first-child"));
});


function toSlide(a_n, a_element) {
	showSlide(slideIndex = a_n, a_element);
}

function showSlide(a_n, a_element) 
{
	var slides = $(a_element).parent().parent().children(".slideshow").children(".slide");
	var dots = $(a_element).parent().children(".dot");
	if(a_n > slides.length)
	{
		a_n = 1;
	}
	else if(a_n < 1)
	{
		a_n = slides.length;
	}
	slides.css("display", "none");
	dots.removeClass(" activeSlide");
	slides.eq(a_n - 1).css("display", "block");
	dots.eq(a_n - 1).addClass("activeSlide");

	if($(a_element).parent().data("m_timeoutHandle") != null)
	{
		clearTimeout($(a_element).parent().data("m_timeoutHandle"));
		$(a_element).parent().data("m_timeoutHandle", null);
	}

	$(a_element).parent().data("m_timeoutHandle", setTimeout(function()
	{
		showSlide(++a_n, a_element);
	}, 4000));
}