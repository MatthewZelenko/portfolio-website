//---------------------------------------------------------------------------------------------------------MENU---------------------------------------------------------------------------------------------------------
$("#playBtn").click(function()
{
	$("html, body").animate({
		scrollTop: $("#game").offset().top
	}, 1000);
	Game.m_inPauseMenu = false;
});
$("#exitGameBtn").click(function()
{
	$("html, body").animate({
		scrollTop: 0
	}, 1000);
	Game.m_inPauseMenu = true;
});
//---------------------------------------------------------------------------------------------------------MENU---------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------GAMEOBJECTS---------------------------------------------------------------------------------------------------------
function GameObject(a_imagePath, a_x, a_y, a_canBob) 
{
	this.m_image = new Image();
	this.m_image.src = a_imagePath;
	this.m_imagePath = a_imagePath;
	this.m_position = [a_x === undefined ? 0 : a_x, a_y === undefined ? 0 : a_y];
	this.m_initialPosition = this.m_position;
	this.m_elapsedTime = (Math.random() * 2.0) - 1.0;
	this.m_canBob = a_canBob !== undefined ? a_canBob : false;
	this.Update = function(a_deltaTime)
	{
		if(this.m_canBob)
		{
			this.m_elapsedTime += a_deltaTime * 0.25;
			this.m_position[1] = this.m_initialPosition[1] - Math.sin(this.m_elapsedTime) * 0.01;
		}
	};
	this.Draw = function(a_gamePosition, a_parallaxScale) 
	{
		if(a_parallaxScale === undefined)
			a_parallaxScale = 1.0;		
		if(a_gamePosition === undefined)
			a_gamePosition = 0.0;
		context.drawImage(this.m_image, this.m_position[0] - (a_gamePosition * a_parallaxScale), this.m_position[1], this.m_image.width, this.m_image.height);
	};
}
function GameObjectFlipped(a_imagePath, a_x, a_y, a_canBob) 
{
	this.m_image = new Image();
	this.m_image.src = a_imagePath;
	this.m_imagePath = a_imagePath;
	this.m_position = [a_x === undefined ? 0 : a_x, a_y === undefined ? 0 : a_y];
	this.m_initialPosition = [a_x === undefined ? 0 : a_x, a_y === undefined ? 0 : a_y];
	this.m_flipped = false;
	this.m_elapsedTime = ((Math.random() * 2.0) - 1.0);
	this.m_canBob = a_canBob !== undefined ? a_canBob : false;
	this.Update = function(a_deltaTime)
	{
		if(this.m_canBob)
		{
			this.m_elapsedTime += a_deltaTime * 0.25;
			this.m_position[1] = this.m_initialPosition[1] + Math.sin(this.m_elapsedTime);
		}
	};
	this.Draw = function(a_gamePosition, a_parallaxScale) 
	{
		if(a_parallaxScale === undefined)
			a_parallaxScale = 1.0;		
		if(a_gamePosition === undefined)
			a_gamePosition = 0.0;
		if(!this.m_flipped)
		{
			context.drawImage(this.m_image, 0, 0, 0.5 * this.m_image.width, this.m_image.height, this.m_position[0] - (a_gamePosition * a_parallaxScale), this.m_position[1], this.m_image.width * 0.5, this.m_image.height);
		}
		else
		{
			context.drawImage(this.m_image, 0.5 * this.m_image.width, 0, 0.5 * this.m_image.width, this.m_image.height, this.m_position[0] - (a_gamePosition * a_parallaxScale), this.m_position[1], this.m_image.width * 0.5, this.m_image.height);
		}
	};
}
function Bubble(a_imagePath, a_parent, a_xPos, a_yPos, a_url) 
{
	this.m_image = new Image();
	this.m_image.src = a_imagePath;
	this.m_imagePath = a_imagePath;
	this.m_parent = a_parent;
	this.m_position = [a_xPos, a_yPos];
	this.m_url = a_url === undefined ? null : a_url;
	this.m_isActive = false;
	this.m_opacity = 0.0;
	this.Update = function(a_deltaTime)
	{
		if(!this.m_isActive)
		{
			if(this.m_opacity  > 0.0)
			{			
				this.m_opacity -= a_deltaTime * 0.2;
				if(this.m_opacity < 0)
					this.m_opacity = 0;
			}
		}
		else
		{
			this.m_opacity = Math.min(this.m_opacity + (a_deltaTime * 0.2), 1.0);
		}
	}
	this.Draw = function(a_gamePosition)
	{		
		if(a_gamePosition === undefined)
			a_gamePosition = 0.0;
		if(this.m_opacity > 0.0)
		{			
			context.globalAlpha = this.m_opacity;
			context.drawImage(this.m_image, this.m_position[0] - a_gamePosition, this.m_position[1], this.m_image.width, this.m_image.height);
			context.globalAlpha = 1.0;
		}
	}
}

function Pattern(a_x, a_y, a_width, a_height, a_opacity)
{
	this.m_pattern = null;
	this.m_position = [a_x, a_y];
	this.m_size = [a_width, a_height];
	this.m_opacity = (a_opacity === undefined ? 1.0 : a_opacity);
	this.m_lastDraw = true;

	this.Draw = function(a_gamePosition, a_parallaxScale)
	{
		if(a_parallaxScale === undefined)
			a_parallaxScale = 0.0;		
		if(a_gamePosition === undefined)
			a_gamePosition = 0.0;

		if(this.m_opacity >= 0.0 || this.m_lastDraw)
		{			
			context.save();
			context.globalAlpha = this.m_opacity;
			context.fillStyle = this.m_pattern;
			context.translate(this.m_position[0] - (a_gamePosition * a_parallaxScale), this.m_position[1]);
			context.fillRect(0, 0, this.m_size[0], this.m_size[1]);
			context.restore();
			if(this.m_opacity == 0.0 && this.m_lastDraw)
				this.m_lastDraw = false;
		}
	}
}
//---------------------------------------------------------------------------------------------------------GAMEOBJECTS---------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------GAME---------------------------------------------------------------------------------------------------------
var canvas = document.getElementById("mainCanvas");
var context = canvas.getContext("2d");

var Game = 
{
	m_inPauseMenu: true,
	m_time: Date.now(),
	m_gameTime: 0.0,
	m_deltaTime: 0.0,
	m_velocity: 0,
	m_maxVelocity: 50,
	m_gamePosition: 0,
	m_gameMax: 1800,
	m_screenToGame: [0, 0],
	m_dayTime: true,
	m_oldMousePosition: 0,
	m_canvasOffset: [0, 0]
}
var GameObjects = 
{
	m_seaPattern: new Pattern(0, canvas.height - 16, Game.m_gameMax * 2, 27),
	m_skyPattern: new Pattern(0, canvas.height - 128, canvas.width, 128),	m_skyPattern: new Pattern(0, canvas.height - 128, canvas.width, 128),
	m_morningPattern: new Pattern(0, canvas.height - 128, canvas.width, 128, 0.0),
	m_nightPattern: new Pattern(0, 0, canvas.width, canvas.height, 1.0),
	m_sun: new GameObject("images/Sun.png", 50, canvas.height),
	m_moon: new GameObject("images/Moon.png", 50, canvas.height),
	m_title: new GameObject("images/Title.png", canvas.width * 0.5 - 95 * 0.5, canvas.height * 0.25 - 25 * 0.5),
	m_mountains1Pattern: new Pattern(0, canvas.height - 128, Game.m_gameMax, 128),
	m_mountains2Pattern: new Pattern(0, canvas.height - 128, Game.m_gameMax, 128),
	m_boat: new GameObjectFlipped("images/Boat.png", 80, canvas.height - 45, true),
	m_docks: new GameObject("images/Docks.png", 0, canvas.height - 106),
	m_statue: new GameObject("images/Statue.png", 460, canvas.height - 106),
	m_treehouse: new GameObject("images/Treehouse.png", 900, canvas.height - 167),
	m_fmodBarrel: new GameObject("images/Barrel.png", 350, canvas.height - 28, true),
	m_graphicsBottle: new GameObject("images/Bottle.png", 620, canvas.height - 22, true),
	m_aiBottle: new GameObject("images/Bottle.png", 1200, canvas.height - 22, true),
	m_bitbucketBarrel: new GameObject("images/Barrel.png", 1550, canvas.height - 28, true),	
	m_comingSoon: new GameObject("images/Coming Soon.png", 1720, canvas.height - 45)
}
var Bubbles = 
{
	m_currentBubble: null,
	m_bubbles: [
	m_docksBubble = new Bubble("images/DocksBubble.png", GameObjects.m_docks, 25, 44),
	m_statueBubble =  new Bubble("images/StatueBubble.png", GameObjects.m_statue, 459, 40, "aboutme.html"),
	m_treehouseBubble =  new Bubble("images/TreehouseBubble.png", GameObjects.m_treehouse, 952, 75, "https://drive.google.com/open?id=0BzmWDCip_PJLVy1rZkY1NTY3WUk"),
	m_fmodBubble =  new Bubble("images/FMODBubble.png", GameObjects.m_fmodBarrel, 328, 90, "projects.html#fmod"),
	m_graphicsBubble =  new Bubble("images/GraphicsBubble.png", GameObjects.m_graphicsBottle, 598, 90, "projects.html#graphics"),
	m_aiBubble =  new Bubble("images/AiBubble.png", GameObjects.m_aiBottle, 1178, 90, "projects.html#ai"),
	m_bitbucketBubble =  new Bubble("images/BitbucketBubble.png", GameObjects.m_bitbucketBarrel, 1528, 90, "https://bitbucket.org/MatthewZelenko")
	]
}

function Init()
{
	context.mozImageSmoothingEnabled = false;
	context.msImageSmoothingEnabled = false;
	context.imageSmoothingEnabled = false;
	if($(document).scrollTop() > 0)
		Game.m_inPauseMenu = false;
	ResizeGame();
	Game.m_canvasOffset = [$(canvas).offset().left, $(canvas).offset().top];
	window.addEventListener('resize', ResizeGame, false);
	CreatePattern("m_seaPattern", "images/Sea.png", "repeat-x");
	CreatePattern("m_skyPattern", "images/Day Sky.png", "repeat-x");
	CreatePattern("m_morningPattern", "images/Morning Sky.png", "repeat-x");
	CreatePattern("m_nightPattern", "images/Night Sky.png", "repeat-x");
	CreatePattern("m_mountains1Pattern", "images/Mountains1.png", "repeat-x");
	CreatePattern("m_mountains2Pattern", "images/Mountains2.png", "repeat-x");
	$(canvas).on("mousedown", function(a_event)
	{
		a_event.preventDefault();
		Game.m_oldMousePosition = [a_event.originalEvent.pageX - Game.m_canvasOffset[0], a_event.originalEvent.pageY - Game.m_canvasOffset[1]];
		TapBubble(Game.m_oldMousePosition[0] * Game.m_screenToGame[0], Game.m_oldMousePosition[1] * Game.m_screenToGame[1]);
		$(canvas).on("mousemove", function(a_event)
		{
			a_event.preventDefault();

			var	newPosition = [a_event.originalEvent.pageX - Game.m_canvasOffset[0], a_event.originalEvent.pageY - Game.m_canvasOffset[1]];

			Game.m_velocity -= (newPosition[0] - Game.m_oldMousePosition[0])  * 0.1;
			if(Game.m_velocity > Game.m_maxVelocity)
				Game.m_velocity = Game.m_maxVelocity;
			else if(Game.m_velocity < -Game.m_maxVelocity)
				Game.m_velocity = -Game.m_maxVelocity;
			Game.m_oldMousePosition = newPosition;
		});		
		$(canvas).on("mouseup mouseleave", function(a_event)
		{
			a_event.preventDefault();
			$(canvas).off("mousemove");
			$(canvas).off("mouseup mouseleave");
		});
	});

	$(canvas).on("touchstart", function(a_event)
	{
		a_event.preventDefault();
		console.log(a_event.originalEvent.touches);
		Game.m_oldMousePosition = [a_event.originalEvent.touches[0].pageX - Game.m_canvasOffset[0], a_event.originalEvent.touches[0].pageY - Game.m_canvasOffset[1]];
		TapBubble(Game.m_oldMousePosition[0] * Game.m_screenToGame[0], Game.m_oldMousePosition[1] * Game.m_screenToGame[1]);
		$(canvas).on("touchmove", function(a_event)
		{
			a_event.preventDefault();

			var	newPosition = [a_event.originalEvent.touches[0].pageX - Game.m_canvasOffset[0], a_event.originalEvent.touches[0].pageY - Game.m_canvasOffset[1]];

			Game.m_velocity -= (newPosition[0] - Game.m_oldMousePosition[0])  * 0.2;
			if(Game.m_velocity > Game.m_maxVelocity)
				Game.m_velocity = Game.m_maxVelocity;
			else if(Game.m_velocity < -Game.m_maxVelocity)
				Game.m_velocity = -Game.m_maxVelocity;
			Game.m_oldMousePosition = newPosition;
		});		
		$(canvas).on("touchend touchcancel", function(a_event)
		{
			a_event.preventDefault();
			$(canvas).off("touchmove");
			$(canvas).off("touchend touchcancel");
		});
	});



	if(localStorage["boatPosition"] !== undefined)
		GameObjects.m_boat.m_position[0] = parseFloat(localStorage["boatPosition"]);
	if(localStorage["dayTime"])
	{
		Game.m_dayTime = localStorage["dayTime"] === "true" ? true : false;
		if(localStorage["dayPosition"])
		{
			if(Game.m_dayTime == true)
				GameObjects.m_sun.m_position[1] = localStorage["dayPosition"];
			else
				GameObjects.m_moon.m_position[1] = localStorage["dayPosition"];
		}
	}
	UpdateBoat(true);
}
function Update() 
{
	UpdateBoat();
	UpdateBubbles();
	UpdateDayTime();
	UpdateGameobjects();
}
function Render() 
{
	context.clearRect(0, 0, canvas.width, canvas.height);
	GameObjects.m_skyPattern.Draw();
	GameObjects.m_morningPattern.Draw();
	GameObjects.m_nightPattern.Draw();
	GameObjects.m_sun.Draw(Game.m_gamePosition, 0.0);
	GameObjects.m_moon.Draw(Game.m_gamePosition, 0.0);
	GameObjects.m_title.Draw(Game.m_gamePosition, 0.01);
	GameObjects.m_mountains2Pattern.Draw(Game.m_gamePosition, 0.05);
	GameObjects.m_mountains1Pattern.Draw(Game.m_gamePosition, 0.125);
	GameObjects.m_docks.Draw(Game.m_gamePosition);
	GameObjects.m_statue.Draw(Game.m_gamePosition);
	GameObjects.m_treehouse.Draw(Game.m_gamePosition);
	GameObjects.m_comingSoon.Draw(Game.m_gamePosition);
	GameObjects.m_boat.Draw(Game.m_gamePosition);
	GameObjects.m_fmodBarrel.Draw(Game.m_gamePosition);
	GameObjects.m_graphicsBottle.Draw(Game.m_gamePosition);
	GameObjects.m_aiBottle.Draw(Game.m_gamePosition);
	GameObjects.m_bitbucketBarrel.Draw(Game.m_gamePosition);
	GameObjects.m_seaPattern.Draw(Game.m_gamePosition, 1.5);
	DrawBubbles();
}

function UpdateBoat(a_force) 
{
	GameObjects.m_boat.m_position[1] = GameObjects.m_boat.m_initialPosition[1] + Math.sin(Game.m_gameTime * 0.3) * 0.025;

	if(Game.m_velocity == 0 && !a_force || false)
		return;

	if(Game.m_velocity < 0)
	{
		GameObjects.m_boat.m_flipped = true;
	}
	else
	{
		GameObjects.m_boat.m_flipped = false;
	}

	GameObjects.m_boat.m_position[0] += Game.m_velocity * Game.m_deltaTime;
	Game.m_velocity *= 0.99;	//Dampening
	if(Math.abs(Game.m_velocity) < 0.1)		//Threshold
		Game.m_velocity = 0;
	if(GameObjects.m_boat.m_position[0] < 80)
	{
		GameObjects.m_boat.m_position[0] = 80;
		Game.m_velocity = 0;
	}
	else if(GameObjects.m_boat.m_position[0] > Game.m_gameMax - (GameObjects.m_boat.m_image.width * 0.5))
	{
		GameObjects.m_boat.m_position[0] = Game.m_gameMax - (GameObjects.m_boat.m_image.width * 0.5);
		Game.m_velocity = 0;
	}

	Game.m_gamePosition = Math.max(Math.min(GameObjects.m_boat.m_position[0] + (GameObjects.m_boat.m_image.width * 0.25) - (canvas.width * 0.5), Game.m_gameMax - canvas.width), 0);
	CheckCollision();

	localStorage["boatPosition"] = String(GameObjects.m_boat.m_position[0]);
}
function UpdateGameobjects() {
	GameObjects.m_boat.Update(Game.m_deltaTime);
	GameObjects.m_aiBottle.Update(Game.m_deltaTime);
	GameObjects.m_graphicsBottle.Update(Game.m_deltaTime);
	GameObjects.m_fmodBarrel.Update(Game.m_deltaTime);
	GameObjects.m_bitbucketBarrel.Update(Game.m_deltaTime);
}
//BUBBLES
function CheckCollision() 
{
	var boatCenter = GameObjects.m_boat.m_position[0] + GameObjects.m_boat.m_image.width * 0.25;
	if(boatCenter >= GameObjects.m_docks.m_position[0] && boatCenter <= GameObjects.m_docks.m_position[0] + GameObjects.m_docks.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[0])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[0];
		}
	}
	else if(boatCenter >= GameObjects.m_statue.m_position[0] && boatCenter <= GameObjects.m_statue.m_position[0] + GameObjects.m_statue.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[1])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[1];
		}
	}
	else if(boatCenter >= GameObjects.m_treehouse.m_position[0] && boatCenter <= GameObjects.m_treehouse.m_position[0] + GameObjects.m_treehouse.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[2])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[2];
		}
	}
	else if(boatCenter >= GameObjects.m_fmodBarrel.m_position[0] && boatCenter <= GameObjects.m_fmodBarrel.m_position[0] + GameObjects.m_fmodBarrel.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[3])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[3];
		}
	}
	else if(boatCenter >= GameObjects.m_graphicsBottle.m_position[0] && boatCenter <= GameObjects.m_graphicsBottle.m_position[0] + GameObjects.m_graphicsBottle.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[4])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[4];
		}
	}
	else if(boatCenter >= GameObjects.m_aiBottle.m_position[0] && boatCenter <= GameObjects.m_aiBottle.m_position[0] + GameObjects.m_aiBottle.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[5])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[5];
		}
	}
	else if(boatCenter >= GameObjects.m_bitbucketBarrel.m_position[0] && boatCenter <= GameObjects.m_bitbucketBarrel.m_position[0] + GameObjects.m_bitbucketBarrel.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[6])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[6];
		}
	}
	else
	{
		if(Bubbles.m_currentBubble)
			Bubbles.m_currentBubble.m_isActive = false;
		Bubbles.m_currentBubble = null;
	}
	if(Bubbles.m_currentBubble)
		Bubbles.m_currentBubble.m_isActive = true;
}
function UpdateBubbles()
{	
	for(var i = 0; i < 7; ++i)
	{
		Bubbles.m_bubbles[i].Update(Game.m_deltaTime);
	}
}
function DrawBubbles() 
{
	for(var i = 0; i < 7; ++i)
	{
		Bubbles.m_bubbles[i].Draw(Game.m_gamePosition);
	}
}
function TapBubble(a_locationX, a_locationY) 
{
	for(var i = 1; i < 7; ++i)
	{
		var bubble = Bubbles.m_bubbles[i];
		if(!bubble.m_isActive || bubble.m_opacity < 0.75 || !bubble.m_url)
			continue;

		var touchLocation = [a_locationX + Game.m_gamePosition, a_locationY];
		if(touchLocation[0] > bubble.m_position[0] &&
			touchLocation[0] < bubble.m_position[0] + bubble.m_image.width &&
			touchLocation[1] > bubble.m_position[1] &&
			touchLocation[1] < bubble.m_position[1]+ bubble.m_image.height)
		{
			window.open(bubble.m_url, "_self");
		}
	}
}

//DAYTIME
function UpdateDayTime() 
{
	if(Game.m_dayTime)
	{
		GameObjects.m_sun.m_position[1] -= Game.m_deltaTime * 2;

		//MORNING SKY
		if(GameObjects.m_sun.m_position[1] >= -GameObjects.m_sun.m_image.height)
		{
			GameObjects.m_morningPattern.m_opacity = (GameObjects.m_sun.m_position[1] + GameObjects.m_sun.m_image.height) / (canvas.height + GameObjects.m_sun.m_image.height);
		}
		else
		{
			GameObjects.m_morningPattern.m_opacity = 0.0;
		}
		//END MORNING SKY
		//NIGHT SKY
		if(GameObjects.m_sun.m_position[1] <= canvas.height && GameObjects.m_sun.m_position[1] > 0)
		{
			GameObjects.m_nightPattern.m_opacity = GameObjects.m_sun.m_position[1] / canvas.height;
		}
		else
		{
			GameObjects.m_nightPattern.m_opacity = 0.0;
		}
		if(GameObjects.m_sun.m_position[1] <= -canvas.height * 1.5)
		{
			Game.m_dayTime = false;
			GameObjects.m_sun.m_position[1] = canvas.height;
		}
		//END OF NIGHT SKY
	}
	else
	{
		GameObjects.m_moon.m_position[1] -= Game.m_deltaTime * 2;

		//NIGHT SKY APPEAR AT THE START OF NIGHT
		if(GameObjects.m_moon.m_position[1] <= canvas.height)
		{
			GameObjects.m_nightPattern.m_opacity = 1.0 - (GameObjects.m_moon.m_position[1] / canvas.height);
		}
		else
		{
			GameObjects.m_nightPattern.m_opacity = 1.0;
		}
		//END NIGHT SKY

		if(GameObjects.m_moon.m_position[1] <= -canvas.height * 1.5)
		{
			Game.m_dayTime = true;
			GameObjects.m_moon.m_position[1] = canvas.height;
		}
	}
	localStorage["dayTime"] = String(Game.m_dayTime);
	localStorage["dayPosition"] = String(Game.m_dayTime ? GameObjects.m_sun.m_position[1] : GameObjects.m_moon.m_position[1]);
}

//Helpers
function CreatePattern(a_pattern, a_imagePath, a_repeat) 
{
	var image = new Image();
	image.src = a_imagePath;
	image.onload = function()
	{
		GameObjects[a_pattern].m_pattern = context.createPattern(image, a_repeat);
	};
}
function ResizeGame()
{
	var gameArea = $("#gameArea");
	var widthToHeight = 5/2;
	var newWidth = $(window).width();
	var newHeight = $(window).height();
	var newWidthToHeight = newWidth / newHeight;

	if(newWidthToHeight > widthToHeight) 
	{
		newWidth = newHeight * widthToHeight;
	}
	else
	{
		newHeight = newWidth / widthToHeight;
	}	
	gameArea.css("width", newWidth + "px");
	gameArea.css("height", newHeight + "px");

	gameArea.css("marginTop", (-newHeight * 0.5) + "px");
	gameArea.css("marginLeft", (-newWidth * 0.5) + "px");

	Game.m_screenToGame[0] = canvas.width / canvas.clientWidth;
	Game.m_screenToGame[1] = canvas.height / canvas.clientHeight;

	if(Game.m_inPauseMenu)
	{
		scrollTo(0, 0);
	}
	else
	{
		scrollTo(0, $("#game").offset().top);
	}
	Game.m_canvasOffset = [$(canvas).offset().left, $(canvas).offset().top];
}
//Window Ready, not Docuement Ready: does not wait for images to be loaded
$(window).on("load", function()
{
	Init();
	setInterval(function()
	{
		var newTime = Date.now();
		Game.m_deltaTime = (newTime - Game.m_time) * 0.01;
		Game.m_gameTime += Game.m_deltaTime;
		Game.m_time = newTime;
		if(!Game.m_inPauseMenu)
		{
			Update();
			Render();
		}
		else
		{			
			if($(document).scrollTop() > 0)
				Game.m_inPauseMenu = false;
		}
	}, 0);
	if(Game.m_mobile)
	{
		setTimeout(function () {
			window.scrollTo(0, 1);
		}, 1000);
	}
});
//---------------------------------------------------------------------------------------------------------GAME---------------------------------------------------------------------------------------------------------