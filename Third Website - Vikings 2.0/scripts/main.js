var game = {
    leftKeyPressed: false,
    rightKeyPressed: false,
    upKeyPressed: false,
    downKeyPressed: false,
    worldStart: 0,
    worldEnd: 0,
    lastX: 0,
    lastY: 0,
    mouseDown: false,
    isSun: true,
    inPage: true
};

$(document).ready(function()
{
    Init();
    $(document).keydown(function(a_event)
    {
        if(a_event.keyCode == 39)
        {
            a_event.preventDefault();
            game.rightKeyPressed = true;
        }
        else if(a_event.keyCode == 37)
        {
            a_event.preventDefault();
            game.leftKeyPressed = true;
        }
        else if(a_event.keyCode == 38)
        {
            a_event.preventDefault();
            game.upKeyPressed = true;
        }
        else if(a_event.keyCode == 40)
        {
            a_event.preventDefault();
            game.downKeyPressed = true;
        }
    });
    $(document).keyup(function(a_event)
    {
        if(a_event.keyCode == 39)
        {
            a_event.preventDefault();
            game.rightKeyPressed = false;
        }
        else if(a_event.keyCode == 37)
        {
            a_event.preventDefault();
            game.leftKeyPressed = false;
        }
        else if(a_event.keyCode == 38)
        {
            a_event.preventDefault();
            game.upKeyPressed = false;
        }
        else if(a_event.keyCode == 40)
        {
            a_event.preventDefault();
            game.downKeyPressed = false;
        }
    });
    //Wheel
    $("#content").on("mousewheel DOMMouseScroll MozMousePixelScroll", function(a_event)
    {
        a_event.preventDefault();
        if(game.inPage)
        {
            ScrollPage(-a_event.originalEvent.wheelDelta || a_event.originalEvent.detail);
        }   
        else
        {     
            MoveBoat(a_event.originalEvent.wheelDelta * 0.2 || -a_event.originalEvent.detail * 0.2);
        }
    });

    //Drag
    $("#content").mousedown(function(a_event)
    {
        a_event.preventDefault();
        if(game.inPage)
        {
            game.lastY = a_event.pageY;
            game.mousedown = true;
        }
        else
        {
            game.lastX = a_event.pageX;
            game.mousedown = true;
        }
    });
    $("#content").mousemove(function(a_event)
    {
        if(game.mousedown)
        {
            if(game.inPage)
            {
                ScrollPage(game.lastY - a_event.pageY);
                game.lastY = a_event.pageY;
            }   
            else
            {  
                MoveBoat((game.lastX - a_event.pageX) * 0.25);
                game.lastX = a_event.pageX;
            }
        }
    });
    $("#content").on("mouseup mouseleave", function(a_event)
    {
        a_event.preventDefault();
        game.mousedown = false;
    });

    //Touch
    $("#content").on("touchstart", function(a_event)
    {
        a_event.preventDefault();
        if(game.inPage)
        {
            game.lastY = a_event.originalEvent.touches[0].clientY;
        }   
        else
        {  
            game.lastX = a_event.originalEvent.touches[0].clientX;
        }
    });
    $("#content").on("touchmove", function(a_event)
    {
        if(game.inPage)
        {
            ScrollPage(game.lastY - a_event.originalEvent.touches[0].clientY);
            game.lastY = a_event.originalEvent.touches[0].clientY;
        }   
        else
        {  
            MoveBoat((game.lastX - a_event.originalEvent.touches[0].clientX) * 0.25);
            game.lastX = a_event.originalEvent.touches[0].clientX
        }
    });

    //Scrolling disabled
    $("#content").scroll(function(a_event)
    {
        a_event.preventDefault();
    });

    //Page
    $("#closePageWrapper").on("click touchstart", function(a_event)
    {
        a_event.preventDefault();
        if(game.inPage)
        {
            $("#pageWrapper").hide();
            $("#welcomeIslandPage").hide();
            $("#aboutMeIslandPage").hide();
            $("#resumeIslandPage").hide();
            game.inPage = false;
        }
    });
    $(".closePage").on("click touchstart", function()
    {
        $(".closePage").parent().parent().hide();
        $("#pageWrapper").hide();
        game.inPage = false;
    });

    $("#welcomeislandButton").on("click touchstart", function()
    {
        $("#pageWrapper").show();
        $("#welcomeIslandPage").show();
        console.log("show");
        game.inPage = true;
    });
    $("#aboutmeislandButton").on("click touchstart", function()
    {
        $("#pageWrapper").show();
        $("#aboutMeIslandPage").show();
        console.log("show");
        game.inPage = true;
    });
    $("#resumeislandButton").on("click touchstart", function()
    {
        $("#pageWrapper").show();
        $("#resumeIslandPage").show();
        console.log("show");
        game.inPage = true;
    });

    //Main
    setInterval(Run, 1000 / 60);
});

function Init()
{
    $("#content").scrollLeft(0);
    game.worldStart = GetParameter("#boat", "left");
    game.worldEnd = GetParameter("#comingsoon", "left") + GetParameter("#comingsoon", "width");
    SetParameter("#sun", "bottom", 0);
    SetParameter("#moon", "bottom", -$(window).height() * 2);
}
function Run()
{
    if(game.inPage)
    {
        if(game.upKeyPressed)
            ScrollPage(-10);
        if(game.downKeyPressed)
            ScrollPage(10);
    }
    else
    {
        if(game.leftKeyPressed)
            MoveBoat(-5);
        if(game.rightKeyPressed)
            MoveBoat(5);
    }
    MoveSunMoon(0.5);
}

function MoveBoat(a_xVelocity)
{
    if(a_xVelocity == 0 || (game.rightKeyPressed && game.leftKeyPressed))
        return;

    var boatLeft = GetParameter("#boat", "left");
    var boatWidth = GetParameter("#boat", "width");
    if(a_xVelocity < 0) //When going left
    {
        var diff = game.worldStart - boatLeft;
        if(diff == 0)
            return;
        if(diff > a_xVelocity) //If the boat is going to exceed to start, Change the velocity
        {
            a_xVelocity = diff;
        }
    }
    else //When going right
    {
        var diff = game.worldEnd - (boatLeft + boatWidth);
        if(diff == 0)
            return;
        if(diff < a_xVelocity) //If the boat is going to exceed to end, Change the velocity
        {
            a_xVelocity = diff;
        }
    }
    //Set boat position
    SetParameter("#boat", "left", boatLeft + a_xVelocity);
    MoveSunMoon(2.5);

    //Set boat scroll
    var preScrollLeft = $("#content").scrollLeft();
    ScollToBoat();
    var postScrollLeft = $("#content").scrollLeft();

    //Scroll bg based on how much scrolling has occured
    var diff = postScrollLeft - preScrollLeft;
    ParallaxBG(diff);
}
function MoveSunMoon(a_xVelocity)
{
    SetParameter("#sun", "bottom", GetParameter("#sun", "bottom") + Math.abs(a_xVelocity));         //Move the sun
    SetParameter("#moon", "bottom", GetParameter("#moon", "bottom") + Math.abs(a_xVelocity));       //Move the moon

    if(game.isSun)
    {        var val = Clamp(0.0, 1.0, GetParameter("#sun", "bottom") / $(window).height() * 2.0);  //Get a value between 0 and 1 from how far the sun has passed the bottom of the screen to the top
        $("#nightSky").css("opacity", 1.0 - val);                                                   //Slowly fade out the night sky
        $("#overlay").css("background-color", "rgba(0, 20, 150, " + (0.25 - (val * 0.25)) + ")");   //Slowly fade out the night overlay

        val = Clamp(0.0, 1.0, GetParameter("#sun", "bottom") / $(window).height() * 0.75);          //Slowly fade out the morning orange glow. but more towards the middle of the day
        $("#morningSky").css("opacity", 1.0 - val);
    }
    else
    {
        var val = Clamp(0.0, 1.0, GetParameter("#moon", "bottom") / $(window).height() * 2.0);      //Get a value between 0 and 1 from how far the moon has passed the bottom of the screen to the top
        $("#nightSky").css("opacity", val);                                                         //Slowly fade in the night sky
        $("#overlay").css("background-color", "rgba(0, 20, 150, " + (val * 0.25) + ")");            //Slowly fade in the night overlay
    }

    if(GetParameter("#sun", "bottom") >= $(window).height() * 2.0)                                  //If the sun is passed double window height, Move it down negative double window height
    {
        SetParameter("#sun", "bottom", -($(window).height() * 2.0));
        game.isSun = false;
    }
    if(GetParameter("#moon", "bottom") >= $(window).height() * 2.0)                                 //If the moon is passed double window height, Move it down negative double window height
    {
        SetParameter("#moon", "bottom", -($(window).height() * 2.0));
        game.isSun = true;
        $("#morningSky").css("opacity", 1.0);                                                           //Set the orange morning glow to full opacity. The night sky will do the fading in for it.
    }
}
function ScollToBoat() 
{
    $("#content").scrollLeft(GetParameter("#boat", "left") + (GetParameter("#boat", "width") * 0.5) - ($(window).width() * 0.5));
}
function ParallaxBG(a_xVelocity)
{
    SetParameter("#sun", "left", GetParameter("#sun", "left") + a_xVelocity);
    SetParameter("#moon", "left", GetParameter("#moon", "left") + a_xVelocity);

    var bgPos = GetParameter("#mountains1", "background-position");
    bgPos[0] += a_xVelocity * 0.6;
    SetParameter("#mountains1", "background-position", bgPos);

    bgPos = GetParameter("#mountains2", "background-position");
    bgPos[0] += a_xVelocity * 0.8;
    SetParameter("#mountains2", "background-position", bgPos);

    SetParameter("#title", "left", GetParameter("#title", "left") + (a_xVelocity* 0.9));

    bgPos = GetParameter("#nightSky", "background-position");
    bgPos[0] += a_xVelocity;
    SetParameter("#nightSky", "background-position", bgPos);

}
function ScrollPage(a_yVelocity) 
{
    $("#pageWrapper").scrollTop($("#pageWrapper").scrollTop() + a_yVelocity);
}

//Helper Functions
function GetParameter(a_id, a_parameter)
{
    var arr = $(a_id).css(a_parameter).split(" ");
    for (var i = 0; i < arr.length; i++)
    {
        arr[i] = parseFloat(arr[i]);
    }
    if(arr.length == 1)
    {
        arr = arr[0];
    }
    return arr;
}
function SetParameter(a_id, a_parameter, a_value)
{
    var result;
    if(a_value.constructor === Array)
    {
        result = a_value[0] + "px";
        for (var i = 1; i < a_value.length; i++)
        {
            result += " " + a_value[i] + "px";
        }
    }
    else
    {
        result = a_value;
    }
    $(a_id).css(a_parameter, result);
}
function Lerp(a_x, a_y, a_t) 
{
    return ((1 - a_t) * a_x) + (a_t * a_y);
}
function Clamp(a_x, a_y, a_value) {
    return Math.max(a_x, Math.min(a_y, a_value));
}