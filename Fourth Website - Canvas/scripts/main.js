var Game = 
{
	m_mobile: false,
	m_canvasOffset: null,
	m_lastTouch: 0,
	m_controls: {
		m_leftIsPressed: false,
		m_rightIsPressed: false
	},
	m_time: Date.now(),
	m_gameTime: 0.0,
	m_deltaTime: 0.0,
	m_velocity: 0,
	m_maxVelocity: 50,
	m_gamePosition: 0,
	m_gameMax: 1202,
	m_screenToGame: [0, 0],
	m_dayTime: true
}
var GameObjects = 
{
	m_seaPattern: new Pattern(0, canvas.height - 27, Game.m_gameMax * 2, 27),
	m_skyPattern: new Pattern(0, canvas.height - 128, canvas.width, 128),	m_skyPattern: new Pattern(0, canvas.height - 128, canvas.width, 128),
	m_morningPattern: new Pattern(0, canvas.height - 128, canvas.width, 128, 0.0),
	m_nightPattern: new Pattern(0, 0, canvas.width, canvas.height, 1.0),
	m_sun: new GameObject("images/Sun.png", 50, canvas.height),
	m_moon: new GameObject("images/Moon.png", 50, canvas.height),
	m_title: new GameObject("images/Title.png", canvas.width * 0.5 - 95 * 0.5, canvas.height * 0.25 - 25 * 0.5),
	m_mountains1Pattern: new Pattern(0, canvas.height - 128, Game.m_gameMax, 128),
	m_mountains2Pattern: new Pattern(0, canvas.height - 128, Game.m_gameMax, 128),
	m_boat: new GameObjectFlipped("images/Boat.png", 100, canvas.height - 45 - 10),
	m_docks: new GameObject("images/Docks.png", 22, canvas.height - 106 - 10),
	m_statue: new GameObject("images/Statue.png", 400 + 22, canvas.height - 106 - 10),
	m_treehouse: new GameObject("images/Treehouse.png", 900 + 22, canvas.height - 167 - 10),
	m_comingSoon: new GameObject("images/Coming Soon.png", 1100 + 22, canvas.height - 45 - 10)
}
var Bubbles = 
{
	m_currentBubble: null,
	m_bubbles: [
	m_docksBubble = new Bubble("images/DocksBubble.png", GameObjects.m_docks, 47, 34),
	m_statueBubble =  new Bubble("images/StatueBubble.png", GameObjects.m_statue, 421, 30, "aboutme.html"),
	m_treehouseBubble =  new Bubble("images/TreehouseBubble.png", GameObjects.m_treehouse, 974, 65, "resume.html")
	]
}

function Init()
{
	if($.browser.mobile)
	{
		Game.m_mobile = true;
		console.log("true");
		canvasOffset = $(canvas).offset();
	}
	ResizeGame();
	window.addEventListener('resize', ResizeGame, false);
	CreatePattern("m_seaPattern", "images/Sea.png", "repeat-x");
	CreatePattern("m_skyPattern", "images/Day Sky.png", "repeat-x");
	CreatePattern("m_morningPattern", "images/Morning Sky.png", "repeat-x");
	CreatePattern("m_nightPattern", "images/Night Sky.png", "repeat-x");
	CreatePattern("m_mountains1Pattern", "images/Mountains1.png", "repeat-x");
	CreatePattern("m_mountains2Pattern", "images/Mountains2.png", "repeat-x");

	if(!Game.m_mobile)
	{
		$(canvas).on("mousedown", function(a_event)
		{
			TapBubble(a_event.offsetX * Game.m_screenToGame[0], a_event.offsetY * Game.m_screenToGame[1]);
			$(canvas).on("mousemove", function(a_event)
			{
				Game.m_velocity -= a_event.originalEvent.movementX * 0.1;
				if(Game.m_velocity > Game.m_maxVelocity)
					Game.m_velocity = Game.m_maxVelocity;
				else if(Game.m_velocity < -Game.m_maxVelocity)
					Game.m_velocity = -Game.m_maxVelocity;
			});		
			$(canvas).on("mouseup mouseleave", function(a_event)
			{
				$(canvas).off("mousemove");
				$(canvas).off("mouseup mouseleave");
			});
		});
	}
	else
	{
		$(canvas).on("touchstart", function(a_event)
		{
			var offset = [0, 0];
			offset[0] = a_event.touches[0].pageX - Game.m_canvasOffset.left;
			offset[1] = a_event.touches[1].pageY - Game.m_canvasOffset.top;
			TapBubble(offset[0] * Game.m_screenToGame[0], offset[1] * Game.m_screenToGame[1]);
			Game.m_lastTouch = offset[0];
		});
		$(canvas).on("touchmove", function(a_event)
		{
			var newTouch = a_event.touches[0].pageX - Game.m_canvasOffset.left;
			var movementX = newTouch - Game.m_lastTouch;

			Game.m_velocity -= movementX * 0.1;
			if(Game.m_velocity > Game.m_maxVelocity)
				Game.m_velocity = Game.m_maxVelocity;
			else if(Game.m_velocity < -Game.m_maxVelocity)
				Game.m_velocity = -Game.m_maxVelocity;

			Game.m_lastTouch = newTouch;
		});
	}

	if(localStorage["boatPosition"])
		GameObjects.m_boat.m_position[0] = parseFloat(localStorage["boatPosition"]);
	if(localStorage["dayTime"])
	{
		Game.m_dayTime = localStorage["dayTime"] === "true" ? true : false;
		if(localStorage["dayPosition"])
		{
			if(Game.m_dayTime == true)
				GameObjects.m_sun.m_position[1] = localStorage["dayPosition"];
			else
				GameObjects.m_moon.m_position[1] = localStorage["dayPosition"];
		}
	}
	UpdateBoat(true);
}
function Update() 
{
	UpdateBoat();
	UpdateBubbles();
	UpdateDayTime();
}
function Render() 
{
	context.clearRect(0, 0, canvas.width, canvas.height);
	GameObjects.m_skyPattern.Draw();
	GameObjects.m_morningPattern.Draw();
	GameObjects.m_nightPattern.Draw();
	GameObjects.m_sun.Draw(Game.m_gamePosition, 0.0);
	GameObjects.m_moon.Draw(Game.m_gamePosition, 0.0);
	GameObjects.m_title.Draw(Game.m_gamePosition, 0.01);
	GameObjects.m_mountains2Pattern.Draw(Game.m_gamePosition, 0.05);
	GameObjects.m_mountains1Pattern.Draw(Game.m_gamePosition, 0.125);
	GameObjects.m_docks.Draw(Game.m_gamePosition);
	GameObjects.m_statue.Draw(Game.m_gamePosition);
	GameObjects.m_treehouse.Draw(Game.m_gamePosition);
	GameObjects.m_comingSoon.Draw(Game.m_gamePosition);
	GameObjects.m_boat.Draw(Game.m_gamePosition);
	GameObjects.m_seaPattern.Draw(Game.m_gamePosition, 1.5);
	DrawBubbles();
}

function UpdateBoat(a_force) 
{
	GameObjects.m_boat.m_position[1] = GameObjects.m_boat.m_initialPosition[1] + Math.sin(Game.m_gameTime * 0.3) * 0.025;

	if(Game.m_velocity == 0 && !a_force || false)
		return;

	if(Game.m_velocity < 0)
	{
		GameObjects.m_boat.m_flipped = true;
	}
	else
	{
		GameObjects.m_boat.m_flipped = false;
	}

	GameObjects.m_boat.m_position[0] += Game.m_velocity * Game.m_deltaTime;
	Game.m_velocity *= 0.99;	//Dampening
	if(Math.abs(Game.m_velocity) < 0.1)		//Threshold
		Game.m_velocity = 0;
	if(GameObjects.m_boat.m_position[0] < 100)
	{
		GameObjects.m_boat.m_position[0] = 100;
		Game.m_velocity = 0;
	}
	else if(GameObjects.m_boat.m_position[0] > Game.m_gameMax - (GameObjects.m_boat.m_image.width * 0.5))
	{
		GameObjects.m_boat.m_position[0] = Game.m_gameMax - (GameObjects.m_boat.m_image.width * 0.5);
		Game.m_velocity = 0;
	}

	Game.m_gamePosition = Math.max(Math.min(GameObjects.m_boat.m_position[0] + (GameObjects.m_boat.m_image.width * 0.25) - (canvas.width * 0.5), Game.m_gameMax - canvas.width), 0);
	CheckCollision();

	localStorage["boatPosition"] = String(GameObjects.m_boat.m_position[0]);
}
//BUBBLES
function CheckCollision() 
{
	var boatCenter = GameObjects.m_boat.m_position[0] + GameObjects.m_boat.m_image.width * 0.25;
	if(boatCenter >= GameObjects.m_docks.m_position[0] && boatCenter <= GameObjects.m_docks.m_position[0] + GameObjects.m_docks.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[0])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[0];
		}
	}
	else if(boatCenter >= GameObjects.m_statue.m_position[0] && boatCenter <= GameObjects.m_statue.m_position[0] + GameObjects.m_statue.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[1])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[1];
		}
	}
	else if(boatCenter >= GameObjects.m_treehouse.m_position[0] && boatCenter <= GameObjects.m_treehouse.m_position[0] + GameObjects.m_treehouse.m_image.width)
	{
		if(Bubbles.m_currentBubble != Bubbles.m_bubbles[2])
		{
			if(Bubbles.m_currentBubble)
				Bubbles.m_currentBubble.m_isActive = false;

			Bubbles.m_currentBubble = Bubbles.m_bubbles[2];
		}
	}
	else
	{
		if(Bubbles.m_currentBubble)
			Bubbles.m_currentBubble.m_isActive = false;
		Bubbles.m_currentBubble = null;
	}
	if(Bubbles.m_currentBubble)
		Bubbles.m_currentBubble.m_isActive = true;
}
function UpdateBubbles()
{	
	for(var i = 0; i < 3; ++i)
	{
		Bubbles.m_bubbles[i].Update(Game.m_deltaTime);
	}
}
function DrawBubbles() 
{
	for(var i = 0; i < 3; ++i)
	{
		Bubbles.m_bubbles[i].Draw(Game.m_gamePosition);
	}
}
function TapBubble(a_locationX, a_locationY) 
{
	for(var i = 1; i < 3; ++i)
	{
		var bubble = Bubbles.m_bubbles[i];
		if(!bubble.m_isActive || bubble.m_opacity != 1.0 || !bubble.m_url)
			continue;

		var touchLocation = [a_locationX + Game.m_gamePosition, a_locationY];
		if(touchLocation[0] > bubble.m_position[0] &&
			touchLocation[0] < bubble.m_position[0] + bubble.m_image.width &&
			touchLocation[1] > bubble.m_position[1] &&
			touchLocation[1] < bubble.m_position[1]+ bubble.m_image.height)
		{
			window.open(bubble.m_url, "_self");
		}
	}
}

//DAYTIME
function UpdateDayTime() 
{
	if(Game.m_dayTime)
	{
		GameObjects.m_sun.m_position[1] -= Game.m_deltaTime * 2;

		//MORNING SKY
		if(GameObjects.m_sun.m_position[1] >= -GameObjects.m_sun.m_image.height)
		{
			GameObjects.m_morningPattern.m_opacity = (GameObjects.m_sun.m_position[1] + GameObjects.m_sun.m_image.height) / (canvas.height + GameObjects.m_sun.m_image.height);
		}
		else
		{
			GameObjects.m_morningPattern.m_opacity = 0.0;
		}
		//END MORNING SKY
		//NIGHT SKY
		if(GameObjects.m_sun.m_position[1] <= canvas.height && GameObjects.m_sun.m_position[1] > 0)
		{
			GameObjects.m_nightPattern.m_opacity = GameObjects.m_sun.m_position[1] / canvas.height;
		}
		else
		{
			GameObjects.m_nightPattern.m_opacity = 0.0;
		}
		if(GameObjects.m_sun.m_position[1] <= -canvas.height * 1.5)
		{
			Game.m_dayTime = false;
			GameObjects.m_sun.m_position[1] = canvas.height;
		}
		//END OF NIGHT SKY
	}
	else
	{
		GameObjects.m_moon.m_position[1] -= Game.m_deltaTime * 2;

		//NIGHT SKY APPEAR AT THE START OF NIGHT
		if(GameObjects.m_moon.m_position[1] <= canvas.height)
		{
			GameObjects.m_nightPattern.m_opacity = 1.0 - (GameObjects.m_moon.m_position[1] / canvas.height);
		}
		else
		{
			GameObjects.m_nightPattern.m_opacity = 1.0;
		}
		//END NIGHT SKY

		if(GameObjects.m_moon.m_position[1] <= -canvas.height * 1.5)
		{
			Game.m_dayTime = true;
			GameObjects.m_moon.m_position[1] = canvas.height;
		}
	}
	localStorage["dayTime"] = String(Game.m_dayTime);
	localStorage["dayPosition"] = String(Game.m_dayTime ? GameObjects.m_sun.m_position[1] : GameObjects.m_moon.m_position[1]);
}

//Helpers
function CreatePattern(a_pattern, a_imagePath, a_repeat) 
{
	var image = new Image();
	image.src = a_imagePath;
	image.onload = function()
	{
		GameObjects[a_pattern].m_pattern = context.createPattern(image, a_repeat);
	};
}
function ResizeGame()
{
	var gameArea = $("#gameArea");
	var widthToHeight = 19/12;
	var newWidth = $(document).width();
	var newHeight = $(document).height();
	var newWidthToHeight = newWidth / newHeight;

	if(newWidthToHeight > widthToHeight) 
	{
		newWidth = newHeight * widthToHeight;
		gameArea.css("width", newWidth + "px");
		gameArea.css("height", newHeight + "px");
	}
	else
	{
		newHeight = newWidth / widthToHeight;
		gameArea.css("width", newWidth + "px");
		gameArea.css("height", newHeight + "px");
	}	

	gameArea.css("marginTop", (-newHeight * 0.5) + "px");
	gameArea.css("marginLeft", (-newWidth * 0.5) + "px");

	Game.m_screenToGame[0] = canvas.width / canvas.clientWidth;
	Game.m_screenToGame[1] = canvas.height / canvas.clientHeight;
}
//Window Ready, not Docuement Ready: does not wait for images to be loaded
$(window).on("load", function()
{
	DetectMobile();
	Init();
	setInterval(function()
	{
		var newTime = Date.now();
		Game.m_deltaTime = (newTime - Game.m_time) * 0.01;
		Game.m_gameTime += Game.m_deltaTime;
		Game.m_time = newTime;
		Update();
		Render();
	}, 0);
	if(Game.m_mobile)
	{
		setTimeout(function () {
			window.scrollTo(0, 1);
		}, 1000);
	}
});

function DetectMobile() 
{
	(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
}