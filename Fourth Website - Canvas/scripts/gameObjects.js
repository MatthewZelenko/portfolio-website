function GameObject(a_imagePath, a_x, a_y) 
{
	this.m_image = new Image();
	this.m_image.src = a_imagePath;
	this.m_imagePath = a_imagePath;
	this.m_position = [a_x === undefined ? 0 : a_x, a_y === undefined ? 0 : a_y];
	this.m_initialPosition = this.m_position;
	this.Draw = function(a_gamePosition, a_parallaxScale) 
	{
		if(a_parallaxScale === undefined)
			a_parallaxScale = 1.0;		
		if(a_gamePosition === undefined)
			a_gamePosition = 0.0;
		context.drawImage(this.m_image, this.m_position[0] - (a_gamePosition * a_parallaxScale), this.m_position[1], this.m_image.width, this.m_image.height);
	};
}
function GameObjectFlipped(a_imagePath, a_x, a_y) 
{
	this.m_image = new Image();
	this.m_image.src = a_imagePath;
	this.m_imagePath = a_imagePath;
	this.m_position = [a_x === undefined ? 0 : a_x, a_y === undefined ? 0 : a_y];
	this.m_initialPosition = [a_x === undefined ? 0 : a_x, a_y === undefined ? 0 : a_y];
	this.m_flipped = false;
	this.Draw = function(a_gamePosition, a_parallaxScale) 
	{
		if(a_parallaxScale === undefined)
			a_parallaxScale = 1.0;		
		if(a_gamePosition === undefined)
			a_gamePosition = 0.0;
		if(!this.m_flipped)
		{
			context.drawImage(this.m_image, 0, 0, 0.5 * this.m_image.width, this.m_image.height, this.m_position[0] - (a_gamePosition * a_parallaxScale), this.m_position[1], this.m_image.width * 0.5, this.m_image.height);
		}
		else
		{
			context.drawImage(this.m_image, 0.5 * this.m_image.width, 0, 0.5 * this.m_image.width, this.m_image.height, this.m_position[0] - (a_gamePosition * a_parallaxScale), this.m_position[1], this.m_image.width * 0.5, this.m_image.height);
		}
	};
}
function Bubble(a_imagePath, a_parent, a_xPos, a_yPos, a_url) 
{
	this.m_image = new Image();
	this.m_image.src = a_imagePath;
	this.m_imagePath = a_imagePath;
	this.m_parent = a_parent;
	this.m_position = [a_xPos, a_yPos];
	this.m_url = a_url === undefined ? null : a_url;

	this.m_isActive = false;
	this.m_opacity = 0.0;
	this.Update = function(a_deltaTime)
	{
		if(!this.m_isActive)
		{
			if(this.m_opacity  > 0.0)
			{			
				this.m_opacity -= a_deltaTime * 0.2;
				if(this.m_opacity < 0)
					this.m_opacity = 0;
			}
		}
		else
		{
			this.m_opacity = Math.min(this.m_opacity + (a_deltaTime * 0.2), 1.0);
		}
	}
	this.Draw = function(a_gamePosition)
	{		
		if(a_gamePosition === undefined)
			a_gamePosition = 0.0;
		if(this.m_opacity > 0.0)
		{			
			context.globalAlpha = this.m_opacity;
			context.drawImage(this.m_image, this.m_position[0] - a_gamePosition, this.m_position[1], this.m_image.width, this.m_image.height);
			context.globalAlpha = 1.0;
		}
	}
}

function Pattern(a_x, a_y, a_width, a_height, a_opacity)
{
	this.m_pattern = null;
	this.m_position = [a_x, a_y];
	this.m_size = [a_width, a_height];
	this.m_opacity = (a_opacity === undefined ? 1.0 : a_opacity);
	this.m_lastDraw = true;

	this.Draw = function(a_gamePosition, a_parallaxScale)
	{
		if(a_parallaxScale === undefined)
			a_parallaxScale = 0.0;		
		if(a_gamePosition === undefined)
			a_gamePosition = 0.0;

		if(this.m_opacity >= 0.0 || this.m_lastDraw)
		{			
			context.save();
			context.globalAlpha = this.m_opacity;
			context.fillStyle = this.m_pattern;
			context.translate(this.m_position[0] - (a_gamePosition * a_parallaxScale), this.m_position[1]);
			context.fillRect(0, 0, this.m_size[0], this.m_size[1]);
			context.restore();
			if(this.m_opacity == 0.0 && this.m_lastDraw)
				this.m_lastDraw = false;
		}
	}
}