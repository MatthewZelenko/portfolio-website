function SceneObject(a_id)
{
	this.SetParameter = function(a_property, a_value) 
	{
		$(this.m_id).css(a_property, a_value + "px");
	}
	this.GetParameter = function(a_property)
	{
		return parseFloat($(this.m_id).css(a_property));
	}

	this.m_id = a_id;
	this.m_classes = document.getElementById(this.m_id.substring(1)).className.split(/\s+/);
	this.m_width = this.GetParameter("width");
	this.m_left = this.GetParameter("left");
	this.m_right = this.m_left + this.m_width;

	this.m_height = this.GetParameter("height");
	this.m_bottom = this.GetParameter("bottom");
	this.m_top = this.m_bottom + this.m_height;
	this.m_speed = 1.0;

	this.SetLeft = function(a_value)
	{
		this.m_left = a_value;
		this.m_right = a_value + this.m_width;
		this.SetParameter("left", this.m_left);
	}
	this.AddLeft = function(a_value)
	{
		this.m_left += a_value;
		this.m_right += a_value;
		this.SetParameter("left", this.m_left);
	}
	this.SetBottom = function(a_value)
	{
		this.m_bottom = a_value;
		this.m_top = a_value + this.m_height;
		this.SetParameter("bottom", this.m_bottom);
	}
	this.AddBottom = function(a_value)
	{
		this.m_bottom += a_value;
		this.m_top += a_value;
		this.SetParameter("bottom", this.m_bottom);
	}
	this.HasClass = function(a_class)
	{
		return $(this.m_id).hasClass(a_class);
	}

}

var isTouch = false;
var ready = false;
var resizeTimeout;
//Button presses
var leftIsPressed = false;
var rightIsPressed = false;
var oldUpIsPressed = false;
var upIsPressed = false;
var disabledMovement = false;
var inCollision = false;
var pressUpLeft, pressUpBottom;
//Speed varies depending on the height
var speed;
var doubleSpeed = false;
//Get the boat
var boat;
var boatBottom;
//CurrentX starts at boats left
var currentX;
//Fill gameobject array with elements in the scene that will move
var sceneObjects;
//Get maxX from farmost objects right
var maxX;

$(document).ready(function()
{
	setInterval(Run, 1000/60);
	$(this).keydown(function(event)
	{
		if(event.keyCode == '87' || event.keyCode == '38')
		{
			upIsPressed = true;
			$("#touchTop p").html("");
		}
		if(event.keyCode == '65' || event.keyCode == '37')
		{
			leftIsPressed = true;
		}
		if(event.keyCode == '68' || event.keyCode == '39')
		{
			rightIsPressed = true;
		}
		if(event.keyCode == '16' || event.keyCode == '13')
		{
			doubleSpeed = true;
		}
	});
	$(this).keyup(function(event)
	{
		if(event.keyCode == '87' || event.keyCode == '38')
		{
			upIsPressed = false;
		}
		if(event.keyCode == '65' || event.keyCode == '37')
		{
			leftIsPressed = false;
		}
		if(event.keyCode == '68' || event.keyCode == '39')
		{
			rightIsPressed = false;
		}
		if(event.keyCode == '16' || event.keyCode == '13')
		{
			doubleSpeed = false;
		}
	});

	setTimeout(function()
	{
		$(".page").hide();
		$("#loading p").html("Press Here To Begin!");
		$("#loading").on("click", function(){

		});
		$("#loading").on("touchstart", function(){
			isTouch = true;
			ready = true;
			$("#loading").css("z-index", -1);
			$("#loading").hide();
			$("#touchTop p").html("Touch Up Here For Instructions");

			$("#touchTop").on("touchstart", function(event){
				event.preventDefault;
				upIsPressed = true;		
				$("#touchTop p").html("");
			});
			$("#touchTop").on("touchend", function(){
				upIsPressed = false;		
			});
			$("#touchLeft").on("touchstart", function(event){
				event.preventDefault;
				leftIsPressed = true;		
			});
			$("#touchLeft").on("touchend", function(){
				leftIsPressed = false;		
			});
			$("#touchRight").on("touchstart", function(event){
				event.preventDefault;
				rightIsPressed = true;		
			});
			$("#touchRight").on("touchend", function(){
				rightIsPressed = false;		
			});
		});
		$("#loading").on("mousedown", function(){
			ready = true;
			$("#loading").css("z-index", -1);
			$("#loading").hide();
			console.log(isTouch);
			$("#touchTop p").html("Press Up Key For Instructions");
		});

		speed = $(document).innerWidth() * 0.002;

		sceneObjects = [new SceneObject("#sea"), new SceneObject("#mountains1"), new SceneObject("#mountains2"), new SceneObject("#title"), new SceneObject("#welcomeIsland"), new SceneObject("#aboutMeIsland"),new SceneObject("#resumeIsland"), new SceneObject("#comingSoon")];
		sceneObjects[0].m_speed = 1.9;
		sceneObjects[1].m_speed = 0.25;
		sceneObjects[2].m_speed = 0.125;
		sceneObjects[3].m_speed = 0.025;
		
		boat = new SceneObject("#boat");
		boatBottom = boat.m_bottom;
		currentX = boat.m_left;

		maxX = sceneObjects[sceneObjects.length - 1].m_right - boat.m_width;

		var boatCenter = boat.m_left + (boat.m_width * 0.5);
		pressUpLeft = boatCenter - (parseFloat($("#pressUp").css("width")) * 0.5);
		pressUpBottom = boat.m_top + 10;
		$("#pressUp").css("left", pressUpLeft + "px");
		$("#pressUp").css("bottom", pressUpBottom + "px");
	}, 1500);
});

function Run() 
{
	if(ready)
	{
		Update();
	}
}
function Update()
{
	MoveGameObjects();
	CheckCollisions();
	oldUpIsPressed = upIsPressed;
	PressUp();
}

function MoveGameObjects() 
{
	if(disabledMovement)
		return;
	var x = 0;
	if(leftIsPressed)
	{
		//Can only go left if currentX > far left
		if(currentX > boat.m_left)
			x -= speed * (doubleSpeed ? 2.0 : 1.0);
	}
	if(rightIsPressed)
	{
		//Can only go right if currentX < far right
		if(currentX < maxX)
			x += speed * (doubleSpeed ? 2.0 : 1.0);
	}

	if(x != 0)
	{
		currentX += x;
		//Keep the boat within the boundaries
		if(currentX < boat.m_left)
		{
			x += boat.m_left - currentX;
			currentX = boat.m_left;
		}
		else if(currentX > maxX)
		{
			x += maxX - currentX;
			currentX = maxX;			
		}

		//Update all gameobject in scene
		for (var i = 0; i < sceneObjects.length; i++) {
			var obj = sceneObjects[i];
			obj.AddLeft(-x * obj.m_speed);
		}
		var boatHeight = Math.sin(currentX * 0.04) * 5.0;
		console.log(boatHeight);
		boat.SetParameter("bottom", boatBottom + boatHeight);
	}
}

function CheckCollisions() 
{
	inCollision = false;
	var boatCenter = boat.m_left + (boat.m_width * 0.5);
	for (var i = 1; i < sceneObjects.length; i++) {
		var obj = sceneObjects[i];
		if(!obj.HasClass("sceneObject"))
			continue;
		if(Math.abs(boatCenter - (obj.m_left + (obj.m_width * 0.5))) <= ((obj.m_width * 0.5) + (boat.m_width * 0.5)) * 0.8)
		{
			pressUpLeft = boatCenter - (parseFloat($("#pressUp").css("width")) * 0.5);
			pressUpBottom = boat.m_top + 10;
			inCollision = true;
			if(!oldUpIsPressed && upIsPressed)
			{
				$(obj.m_id + "Page").toggle();
				disabledMovement = !disabledMovement;
			}
		}
	}
}
function PressUp() {
	if(!disabledMovement && inCollision)
	{
		$("#pressUp").show();
		$("#pressUp").css("left", pressUpLeft + "px");
		$("#pressUp").css("bottom", pressUpBottom + "px");
	}
	else		
	{
		$("#pressUp").hide();
	}
}

$(window).resize(function()
{
	clearTimeout(resizeTimeout);
	resizeTimeout = setTimeout(function()
	{
		location.reload()
	}, 250);	
});